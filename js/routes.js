/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 */

define(['./app'], function (app) {
    'use strict';
    return app.config(['$routeProvider', function ($routeProvider, $rootScope) {
        $routeProvider.when('/', {
            templateUrl: 'partials/main.html'
        }).when('/properties', {
            templateUrl: 'partials/props.html'
        }).when('/development', {
            templateUrl: 'partials/dev.html'
        }).when('/select-equip', {
            templateUrl: 'partials/select-equip.html'
        }).when('/energy-calc', {
            templateUrl: 'partials/energy-calc.html'
        });
        $routeProvider.otherwise({
            redirectTo: '/'
        });
    }]).run(['$rootScope', '$templateCache', function ($rootScope, $templateCache) {
        $rootScope.Math = Math;
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    }]);
});
