/**
 * configure RequireJS
 * prefer named modules to long paths, especially for version mgt
 * or 3rd party libraries
 */
require.config({
    baseUrl: './',
    paths: {
        'angular': 'lib/angular/angular',
        'angular-route': 'lib/angular-route/angular-route',
        'domReady': 'lib/requirejs-domready/domReady',
        'CryptoJS': 'lib/cryptojs/sha1',
        'angular-strap': 'lib/angular-strap/dist/angular-strap',
        'angular-animate' : 'lib/angular-animate/angular-animate',
        'angular-strap.tpl': 'lib/angular-strap/dist/angular-strap.tpl'

    },

    /**
     * for libs that either do not support AMD out of the box, or
     * require some fine tuning to dependency mgt'
     */
    shim: {
        'CryptoJS': {
            exports: 'CryptoJS'
        },
        'angular': {
            exports: 'angular',
            deps: ['CryptoJS']
        },
        'angular-route': {
            deps: ['angular']
        },
        'angular-cookies': {
            deps: ['angular']
        },
        'angular-resource': {
            deps: ['angular']
        },
        'angular-strap': {
            deps: ['angular']
        },
        'angular-animate': {
            deps: ['angular']
        },
        'angular-strap.tpl': {
            deps:['angular-strap']
        }
    },

    deps: [
        // kick start application... see bootstrap.js
        'js/bootstrap'
    ]
});
